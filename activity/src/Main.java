import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");

        Scanner in = new Scanner(System.in);

        try {
            int num = in.nextInt();

            if (num < 0) {
                System.out.println("Factorial is undefined for negative numbers.");
            } else {
                int factorial = 1;
                int counter = 1;

                while (counter <= num) {
                    factorial *= counter;
                    counter++;
                }

                System.out.println("Factorial using while loop: " + factorial);

                factorial = 1;

                for (int i = 1; i <= num; i++) {
                    factorial *= i;
                }

                System.out.println("The factorial of " + num + " is " + factorial);
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid integer.");
        }
    }
}